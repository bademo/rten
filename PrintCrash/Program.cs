﻿using System;

namespace PrintCrash
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to PrintCrash, this is a quick utility to look inside of a CrashDump.json file that was made with Rten. Please press any key once you have put the dump file in PrintCrash's install directory.");
            Console.ReadKey();

            try
            {
                Console.WriteLine("\n\n" + Utilities.UJson.PullCrashDump());
                Console.ReadKey();
            }
            catch
            {
                Console.WriteLine("Couldn't open CrashDump.json!");
                Console.ReadKey();
            }
        }
    }
}
