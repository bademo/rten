﻿using System;
using Utilities;

namespace RtenCore
{
    class Program
    {
        static void Main(string[] args)
        {
			Console.WriteLine("Welcome to RtenCore, the universal installer for cross-platform use! To configure what to download, please edit the .json file named 'AppConfig.json'.");
			Console.WriteLine("would you like to edit Appconfig.json first?");
			if (Console.ReadLine().ToLower().StartsWith("y"))
			{
				Utilities.UJson.SetUp();
				Console.WriteLine("This is a testing build and is not complete yet. This current build is almost finished but has no auto-download..");
				Console.Read();
				return;
			}
			if (!File.Exists(Utilities.UJson.jsonLocation))
			{
				Utilities.UJson.SetUp();
				Console.WriteLine("This is a testing build and is not complete yet. This current build is almost finished but has no auto-download..");
				Console.ReadKey();
				return;
			}
            Install.StartInstall();

            if (UJson.PullShouldAutoDownload())
            {
                Console.WriteLine("NOTE: Auto-download is enabled & is starting now. You can press any key to exit the auto-install.");
                Install.InstallTimer();
            }
            else
            {
                Console.WriteLine("NOTE: Not auto-downloading as json config has advised against");
                Console.ReadKey();
            }
        }
    }
}
