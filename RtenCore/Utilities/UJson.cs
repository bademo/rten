﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;

namespace Utilities
{
    public static class UJson
    {
        public static readonly string jsonLocation = string.Format("{0}AppConfig.json", AppDomain.CurrentDomain.BaseDirectory);
		#region setup
		internal static void SetUp()
		{
			PushData config = new PushData();
			config.DownloadTally = 1;
			Console.Write("project name: ");
			config.ProjectName = Console.ReadLine();
			Console.Write("download url: ");
			config.DownloadURL = Console.ReadLine();
			Console.Write("Time in between dls: ");
			try
			{
				config.HoursPerDownload = int.Parse(Console.ReadLine());
			}
			catch
			{
				config.ShouldAutoDelete = true;
				config.ShouldAutoDownload = false;
				config.HoursPerDownload = 1;
			}
			config.ShouldAutoDownload = GetAutoDl();
			config.ShouldAutoDelete = GetAutoDelete();
			try
			{
				File.WriteAllText(jsonLocation, JsonConvert.SerializeObject(config));
			}
			catch
			{
				Console.WriteLine("make sure Appconfig.json was saved succesfully, encountered errors!");
				return;
			}
		}
		#region Looping methods
		private static bool GetAutoDelete()
		{
			Console.Write("Should auto delete(y/n): ");
			{
				string response = Console.ReadLine().ToLower();
				if (response == "y")
				{
					return true;
				}
				else if (response == "n") return false;
				else
				{
					Console.WriteLine("invalid response");
					return GetAutoDelete();
				}
			}
		}

		private static bool GetAutoDl()
		{
			Console.Write("Should auto download(y/n): ");
			{
				string response = Console.ReadLine().ToLower();
				if (response == "y")
				{
					return true;
				}
				else if (response == "n") return false;
				else
				{
					Console.WriteLine("invalid response");
					return GetAutoDl();
				}
			}
		}
		#endregion
		#endregion

		#region Pulling
		private static string PullJson()
        {
            if (shouldPullNormal)
            {
                using (StreamReader reader = new StreamReader(jsonLocation))
                    return JsonConvert.DeserializeObject(reader.ReadToEnd()).ToString();
            }
            else
            {
                using (StreamReader reader = new StreamReader(jsonCrashLocation))
                    return JsonConvert.DeserializeObject(reader.ReadToEnd()).ToString();
            }
        }



		public static string PullProjectName()
        {
            dynamic jsonFile = JObject.Parse(PullJson(true));
            return jsonFile.ProjectName;
        }

        public static string PullDownloadURL()
        {
            dynamic jsonFile = JObject.Parse(PullJson(true));
            return jsonFile.DownloadURL;
        }

        public static int PullDownloadTally()
        {
            dynamic jsonFile = JObject.Parse(PullJson(true));
            return jsonFile.DownloadTally;
        }

        public static int PullHoursPerDownload()
        {
            dynamic jsonFile = JObject.Parse(PullJson(true));
            return jsonFile.HoursPerDownload;
        }

        public static bool PullShouldAutoDownload()
        {
            dynamic jsonFile = JObject.Parse(PullJson(true));
            return jsonFile.ShouldAutoDownload;
        }

        public static bool PullShouldAutoDelete()
        {
            dynamic jsonFile = JObject.Parse(PullJson(true));
            return jsonFile.ShouldAutoDelete;
        }

        public static string PullCrashDump()
        {
            dynamic jsonFile = JObject.Parse(PullJson(false));
            return string.Format("Project name: {0}\n\nError: {1}\n\nShouldAutoDownload: {2}\n\nShouldAutoDelete: {3}", jsonFile.ProjectName, jsonFile.CrashMessage, jsonFile.ShouldAutoDownload.ToString(), jsonFile.ShouldAutoDelete.ToString());
        }
        #endregion

        #region Pushing
        private class PushData
        {
            public string ProjectName { get; set; }
            public string DownloadURL { get; set; }
            public int DownloadTally { get; set; }
            public int HoursPerDownload { get; set; }
            public bool ShouldAutoDownload { get; set; }
            public bool ShouldAutoDelete { get; set; }
        }

        private class PushCrashData
        {
            public string ProjectName { get; set; }
            public string CrashMessage { get; set; }
            public bool ShouldAutoDownload { get; set; }
            public bool ShouldAutoDelete { get; set; }
        }

        public static void PushDownloadTally()
        {
            PushData pushDownloadTally = new PushData
            {
                ProjectName = PullProjectName(),
                DownloadURL = PullDownloadURL(),
                DownloadTally = PullDownloadTally() + 1,
                HoursPerDownload = PullHoursPerDownload(),
                ShouldAutoDownload = PullShouldAutoDownload(),
                ShouldAutoDelete = PullShouldAutoDelete()
            };

            File.WriteAllText(jsonLocation, JsonConvert.SerializeObject(pushDownloadTally));
        }

        public static void PushNew()
        {
            var pushData = new PushData
            {
                ProjectName = "Rten",
                DownloadURL = "https://docs.google.com/uc?export=download&id=1F06qo_6OBTqa0w6EaCySjHD60T1BdsMQ",
                DownloadTally = 1,
                HoursPerDownload = 2,
                ShouldAutoDownload = false,
                ShouldAutoDelete = true
            };

            File.WriteAllText(jsonLocation, JsonConvert.SerializeObject(pushData));
        }

        public static void PushCrashDump(Exception crashMessage)
        {
            var pushCrashData = new PushCrashData
            {
                ProjectName = PullProjectName(),
                CrashMessage = crashMessage.ToString(),
                ShouldAutoDownload = PullShouldAutoDownload(),
                ShouldAutoDelete = PullShouldAutoDelete()
            };

            File.WriteAllText(jsonCrashLocation, JsonConvert.SerializeObject(pushCrashData));
        }
        #endregion
    }
}
